import MainContainer from "../components/MainContainer"

export default function Error() {
    return (
        <MainContainer>
            <h1>
                Ошибка 404<br />
                Нихуя не найдено
            </h1>
        </MainContainer>
    )
};